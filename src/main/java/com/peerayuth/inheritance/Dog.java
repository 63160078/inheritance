/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peerayuth.inheritance;

/**
 *
 * @author Ow
 */
public class Dog extends Animal {
    public Dog(String name, String color) {
        super(name, color,4);
        System.out.println("Dog created");
    }
    
    @Override
    public void walk() {
        super.walk();
        System.out.println("Dog:"+ name + " walk with "+numberOfLegs+" legs.");
    }
    
    
    @Override
    public void speak() {
        super.speak();
        System.out.println("Dog:" + name + " speak > box box!!");
    }
    
}
